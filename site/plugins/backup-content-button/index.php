<?php

Kirby::plugin('adrienpayet/backup-content-button', [
  'fields' => [
    'backup-content-button' => [],
  ],
  'routes' => [
    require_once __DIR__ . '/src/routes/zip-content.php',
  ],
]);
