<?php

return [
  'pattern' => '/zip-content.json',
  'method'  => 'POST',
  'action'  => function () {
      $json = file_get_contents('php://input');
      $data = json_decode($json);

      // Get real path for our folder
      $inputDirPath = realpath(__DIR__ . '/../../../../../content');
      $outputFilePath = __DIR__ . '/../../../../../content/' . $data->fileName;

      try {
          createZip($inputDirPath, $outputFilePath);
          return json_encode([
            'status'  => 'success',
            'message' => 'Archive' . $data->fileName . ' created',
          ]);
      } catch (\Throwable $th) {
          return json_encode([
            'status'  => 'error',
            'message' => 'Can\'t create archive.',
            'details' => $th->getMessage() . ' in ' . $th->getFile() . ' line ' . $th->getLine(),
          ]);
      }
  },
];
