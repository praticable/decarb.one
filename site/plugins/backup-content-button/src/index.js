import BackupContentButton from "./components/BackupContentButton.vue";

window.panel.plugin("adrienpayet/backup-content-button", {
  fields: {
    "backup-content-button": BackupContentButton,
  },
});
