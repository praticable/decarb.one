<?php

Kirby::plugin('adrienpayet/content-size', [
  'pageMethods' => [
    'contentSize' => function () {
      $contentSize = 0;
      $site = site();
      $html = $this->render();
      $dom = new DOMDocument();
      @$dom->loadHTML($html);

      $xpath = new DOMXPath($dom);
      $currentDivs = $xpath->query('//div[contains(@class, "current")]');

      if ($currentDivs->length > 0) {
          $content = $dom->saveHTML($currentDivs->item(0));
          $contentSize += strlen($content);          
      } else {
          throw new Exception("No content div founded.", 1);
      }

      $images = $dom->getElementsByTagName('img');
      foreach ($images as $image) {
          $src = $image->getAttribute('src');
          
          if (!empty($src)) {
              $localSrc = str_replace($site->url(), realpath(__DIR__ . '/../../../'), $src);
              
              $contentSize += filesize($localSrc);
          }
      }
      
      return $contentSize;
    }
  ]
]);