import writerMarks from "./plugins/writerMarks";
import Callout from "./Marks/Callout";

window.panel.plugin("adrienpayet/kirby-writer-callout", {
  use: [writerMarks],
  thirdParty: {
    marks: {
      // Import custom marks from other plugins
      ...(window.panel.plugins.thirdParty.marks || {}),

      // Provide class, not an instance of it
      callout: Callout,
    },
  },
});
