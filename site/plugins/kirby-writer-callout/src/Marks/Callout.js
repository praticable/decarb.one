import Mark from "./Writer/Mark";

export default class Callout extends Mark {
  get button() {
    return {
      icon: "star",
      label: "Callout",
    };
  }

  commands() {
    return () => this.toggle();
  }

  get name() {
    return "callout";
  }

  get schema() {
    return {
      parseDOM: [{ tag: "span" }],
      toDOM: (node) => [
        "span",
        {
          ...node.attrs,
          class: "callout",
        },
        0,
      ],
    };
  }
}
