<?php

function formatSize($bytes)
{
    if ($bytes >= 1_000_000_000) {
        $bytes = number_format($bytes / 1_000_000_000, 2) . ' Go';
    } elseif ($bytes >= 1_000_000) {
        $bytes = number_format($bytes / 1_000_000, 1) . ' Mo';
    } elseif ($bytes >= 1_000) {
        $bytes = number_format($bytes / 1_000, 0) . ' Ko';
    } elseif ($bytes > 1) {
        $bytes = $bytes . ' octets';
    } elseif ($bytes == 1) {
        $bytes = $bytes . ' octet';
    } else {
        $bytes = '0 octets';
    }

    return $bytes;
}

// https://stackoverflow.com/questions/4914750/how-to-zip-a-whole-folder-using-php
function createZip($inputDirPath, $outputFilePath)
{
    // Initialize archive object
    $zip = new ZipArchive();
    $zip->open($outputFilePath, ZipArchive::CREATE | ZipArchive::OVERWRITE);

    // Create recursive directory iterator
    /** @var SplFileInfo[] $files */
    $files = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator($inputDirPath),
        RecursiveIteratorIterator::LEAVES_ONLY
    );

    foreach ($files as $file) {
        // Skip directories (they would be added automatically)
        if (!$file->isDir()) {
            // Get real and relative path for current file
            $filePath = $file->getRealPath();
            $relativePath = substr($filePath, strlen($inputDirPath) + 1);

            // Add current file to archive
            $zip->addFile($filePath, $relativePath);
        }
    }

    // Zip archive will be created only after closing object
    $zip->close();
}
