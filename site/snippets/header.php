<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="<?= url('assets') ?>/css/style.css">
  <?php snippet('meta') ?>
  <?php snippet('favicon') ?>
  <script>
    const csrf = "<?= csrf() ?>";
    const rootUrl = "<?= $site->url() ?>";
  </script>
  <script src="<?= url('assets') ?>/js/script.js" defer></script>
  <title><?= $site->title() ?> <?php e(!$page->isHomePage(), ' - ' . $page->title()) ?></title>
</head>
<body>
  <div id="cursor">
    <span id="target"></span>
    <span id="text"></span>
  </div>
  <?php if ($page->isHomePage()): ?>
    <div id="introduction">
      <h1><span class="hide">de<br class="mobile">carb</span><span class="hide">.<br class="mobile tablet"></span><span class="hide">one</span></h1>
    </div>
  <?php endif ?>
  <main>