<?php

$alt = $block->alt();
$caption = $block->caption();
$crop = $block->crop()->isTrue();
$link = $block->link();
$ratio = $block->ratio()->or('auto');
$src = null;
$srcset = null;

if ($block->location() == 'web') {
    $src = $block->src()->esc();
} elseif ($image = $block->image()->toFile()) {
    $alt = $alt->or($image->alt());
    $src = $image->url();
    $srcset = $image->srcset();
}

?>
<?php if ($src): ?>
<figure<?= Html::attr(['data-ratio' => $ratio, 'data-crop' => $crop], null, ' ') ?>>
  <?php if ($link->isNotEmpty()): ?>
  <a href="<?= Str::esc($link->toUrl()) ?>"
    <?= e(!str_contains($link->toUrl(), $site->url()), ' target="_blank"') ?>>
    <img src="<?= $src ?>"
      alt="<?= $alt->esc() ?>"
      srcset="<?= $srcset ?>">
  </a>
  <?php else: ?>
  <img src="<?= $src ?>"
    alt="<?= $alt->esc() ?>"
    srcset="<?= $srcset ?>">
  <?php endif ?>

  <?php if ($caption->isNotEmpty()): ?>
  <figcaption>
    <?= $caption ?>
  </figcaption>
  <?php endif ?>
  </figure>
  <?php endif ?>