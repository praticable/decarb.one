<section class="collapsable">
  <button class="collapsable__button">
    <h4><?= $block->title() ?></h4>
  </button>
  <div class="collapsable__content">
    <?= $block->blocks()->toBlocks() ?>
  </div>
</section>