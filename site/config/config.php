<?php
return [
  'routes' => [
    [
      'pattern' => 'sitemap.xml',
      'action'  => function() {
          $pages = site()->pages()->index();

          $ignore = kirby()->option('sitemap.ignore', ['error']);

          $content = snippet('sitemap', compact('pages', 'ignore'), true);

          return new Kirby\Cms\Response($content, 'application/xml');
      }
    ],
    [
      'pattern' => 'sitemap',
      'action'  => function() {
        return go('sitemap.xml', 301);
      }
    ]
  ],
  'sitemap.ignore' => ['error'],
  'debug' => true,
  'api' => [
    'basicAuth' => true
  ],
  'markdown' => [
    'extra' => true,
  ],
  'thumbs' => [
    'srcsets' => [
      'default' => [
        '500w' => ['width' => 500, 'quality' => 80],
        '800w' => ['width' => 800, 'quality' => 80],
        '1024w' => ['width' => 1024, 'quality' => 80],
        '1440w' => ['width' => 1440, 'quality' => 80],
        '2048w' => ['width' => 2048, 'quality' => 80]
      ]
    ]
  ],
  'kirbytext' => [
    'image' => [
      'width' => 800,
    ]
  ],
  'hooks' => [
    'page.update:after' => function($newPage) {
      $contentSize = $newPage->contentSize();

      $newPage->update([
        'size' => $contentSize
      ]);
    },
    'site.update:after' => function($newSite) {
      $contentSize = 0;
      $contentSize = $newSite->homePage()->contentSize();

      $newSite->homePage()->update([
        'size' => $contentSize
      ]);
    }
  ],
  'panel' => [
    'css' => 'assets/css/custom-panel.css'
  ]
];