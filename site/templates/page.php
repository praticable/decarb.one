<?php snippet('header') ?>
<div id="first-level" class="layer home" data-title="<?= $site->title() ?>">
<a href="<?= $site->homePage()->url() ?>" class="layer-btn internal-link" title="retourner à l'accueil" data-size="Accueil">Accueil</a>
</div>

<div id="second-level" class="layer current">
  <div class="empty"></div>
  <a href="<?= $page->url() ?>" class="layer-btn unvisible internal-link" title="aller à la page <?= $page->title() ?>" data-size="<?= $page->title() ?>"><?= $page->title() ?></a>
  <div class="content-background">
    <h1><?= $page->title() ?></h1>
    <?php foreach ($page->body()->toLayouts() as $layout): ?>
    <section class="row" id="<?= $layout->id() ?>">
      <?php foreach ($layout->columns() as $column): ?>
      <div class="column" style="--span:<?= $column->span() ?>">
        <div class="blocks">
          <?php foreach($column->blocks() as $block): ?>
              <?= $block ?>
          <?php endforeach ?>
        </div>
      </div>
      <?php endforeach ?>
    </section>
    <?php endforeach ?>
  </div>
</div>
<div id="third-level" class="layer out-screen"></div>
<?php snippet('footer') ?>