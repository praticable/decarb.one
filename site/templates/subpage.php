<?php snippet('header') ?>
<div id="first-level" class="layer home">
<a href="<?= $site->homePage()->url() ?>" class="layer-btn internal-link" data-size="Accueil">Accueil</a>
</div>
<div id="second-level" class="layer">
  <div class="empty"></div>
  <a href="<?= $page->parent()->url() ?>" class="layer-btn internal-link" data-size="<?= $page->parent()->title() ?>"><?= $page->parent()->title() ?></a>
</div>
<div id="third-level" class="layer current">
  <div class="empty"></div>
  <div class="empty"></div>
  <div class="content-background">
    <div class="empty"></div>
    <h1><?= $page->title() ?></h1>
    <?php foreach ($page->body()->toLayouts() as $layout): ?>
    <section class="row" id="<?= $layout->id() ?>">
      <?php foreach ($layout->columns() as $column): ?>
      <div class="column" style="--span:<?= $column->span() ?>">
        <div class="blocks">
          <?php foreach($column->blocks() as $block): ?>
              <?= $block ?>
          <?php endforeach ?>
        </div>
      </div>
      <?php endforeach ?>
    </section>
    <?php endforeach ?>
  </div>
</div>
<?php snippet('footer') ?>