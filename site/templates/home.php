<?php snippet('header') ?>
<div id="first-level" class="layer current home">
  <a href="<?= $site->homePage()->url() ?>" class="layer-btn unvisible internal-link" title="retourner à l'accueil" data-size="Accueil">Accueil</a>
  <div class="content-background">
    <h2 id="logo">decarb.one</h2>
    <?php foreach ($site->body()->toLayouts() as $layout): ?>
    <section class="row" id="<?= $layout->id() ?>">
      <?php foreach ($layout->columns() as $column): ?>
      <div class="column" style="--span:<?= $column->span() ?>">
        <div class="blocks">
          <?php foreach($column->blocks() as $block): ?>
              <?= $block ?>
          <?php endforeach ?>
        </div>
      </div>
      <?php endforeach ?>
    </section>
    <?php endforeach ?>
  </div>
</div>
<div id="second-level" class="layer out-screen">
</div>
<div id="third-level" class="layer out-screen"></div>
<?php snippet('footer') ?>